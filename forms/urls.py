from django.urls import path

from . import views

app_name = 'forms'

urlpatterns = [
    path('', views.signon, name='login'),
    path('profile/',views.home, name='home'),
    path('signout/',views.signout, name='logout'),
    path('signup/', views.signup, name='signup')
]
