from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect, render
from .forms import StoryForm
from django.contrib.auth.models import User


def home(request):
    return render(request, 'home.html')

def signout(request):
    logout(request)
    return redirect('/')

def signon(request):
    if request.method == "POST":
        form = StoryForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('profile/')
    else:
        form = StoryForm()
        context = {
            'form':form
        }
        return render(request,"login.html", context)

def signup(request):
    if request.method == "POST":
        form = StoryForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            try:
                user = User.objects.get(username=username)
                return redirect("register/")
            
            except User.DoesNotExist:
                user = User.objects.create_user(username, None ,password)
                return redirect('/')
    else:
        form = StoryForm()
        context = {
            'form':form
        }
        return render(request,"register.html", context)
