from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse
from selenium import webdriver

class UnitTesting (TestCase):
    def test0_urlHomeExist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test1_urlRegisterExist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'register.html')

    def test2_urlProfileExist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'home.html')

    def test3_testRegister(self):
        response = self.client.post('/signup/', data={'username':'abcd','password':'abc'})
        self.assertEqual(response.status_code, 302)

    def test4_testLogin(self):
        response = self.client.post('/signup/', data={'username':'abcd','password':'abc'})
        response = Client().post('/', data={'username':'abcd','password':'abc'})
        self.assertEqual(response.status_code, 302)
    
    def test5_urlLogoutExist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test6_signOut(self):
        response = Client().get('/signout/')
        self.assertEqual(response.status_code, 302)
    
    def test7_testRegisterExist(self):
        response = self.client.post('/signup/', data={'username':'abcd','password':'abc'})
        response = self.client.post('/signup/', data={'username':'abcd','password':'abc'})
        self.assertEqual(response.status_code, 302)
