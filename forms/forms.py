from django import forms

class StoryForm(forms.Form):
    username = forms.CharField(label='Username', max_length=100, widget=forms.TextInput(attrs={
        'type':'text',
        'class':'form-control',
        'required': True
    }))

    password = forms.CharField(label='Password', max_length=50, widget=forms.TextInput(attrs={
        'type':'password',
        'class':'form-control',
        'required':True
    }))
